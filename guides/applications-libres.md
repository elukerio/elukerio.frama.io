# Annuaire d'applications Android libres
Les applications présentées sont toutes libres et presque en totalité disponibles sur [F-Droid](https://f-droid.org).

# Boutiques d'Applications
## F-Droid : pour les applications libres

Quoi de mieux pour trouver des applications libres que d'utiliser une boutique d'applications qui référence uniquement ces dernières ? Voici F-Droid.
> Disponible sur [f-droid.org](https://f-droid.org/).

## Yalp Store : pour la transition vers F-Droid
Vous ne voulez plus utiliser Google Play, cependant vous avez besoin de télécharger certaines applications disponibles uniquement sur ce service ?
Utilisez Yalp Store, une application libre qui se connecte à Google Play avec un compte commun et minimise les informations que votre smartphone envoie à Google.
> Disponible sur F-Droid

# Les applications par catégories
## Communication
### Messagerie instantanée : Signal
![Signal Logo](/uploads/signal-logo.png "Signal Logo")
[Signal](#) est une application qui vous permettra de *communiquer de manière sécurisée avec vos contacts*, que ce soit par messages, *appels audio ou vidéo*.  Vous pourrez créer facilement des conversations de groupe, partager des fichiers, et vérifier les clefs de chiffrement de vos amis. C'est donc une vraie alternative à des services comme Skype ou Facebook Messenger.
> Disponible sur Google Play

### SMS : Silence
![Silence Logo](/uploads/silence-logo.png "Silence Logo")
[Silence](#) est une application de SMS basée sur le code source de [Signal](#). Son but est de vous permettre d'envoyer des *SMS chiffrés* en toute simplicité, il suffit d'appuyer sur le petit cadenas en haut à droite de l'application pour initier une session chiffrée avec un autre utilisateur de [Silence](#).
> Disponible sur F-Droid et Google Play

## Divertissement
### Newpipe : pour explorer YouTube
Newpipe, une super appli libre pour explorer YouTube avec quelques fonctionnalitées en plus : pas de pub, possibilité de télécharger les vidéos et de les lire en arrière plan.
> Disponible sur F-Droid

### PeerTube : une alternative à YouTube
PeerTube est une alternative libre à YouTube basée sur le principe de fédération. Plus d'informations : [joinpeertube.org](https://joinpeertube.org).
> Disponible sur F-Droid

## Navigation
### OSMAnd
Une application de navigation GPS entièrement libre avec la possibilité de télécharger les cartes pour pouvoir naviguer hors-ligne. Basée sur les cartes d'[OpenStreetMap](https://www.openstreetmap.org).
> Disponible sur F-Droid

### Transportr
Une application qui vous trace un itinéraire complet (prise en compte des horaires) dans les transports en communs. Très utile pour se déplacer en ville.
> Disponible sur F-Droid

## Organisation
### DavDroid, agenda synchronisé
DavDroid permet de synchroniser votre agenda avec un service CalDav libre tel que [framagenda.org](https://framagenda.org) ou encore le service d'Elukerio qui sera bientôt disponible.
> Disponible sur F-Droid et sur Google Play

### Scarlet (Material Notes), prise de notes
Scarlet (anciennement Material Notes) est une application de prise de notes simple et agréable à utiliser. Elle permet de verrouiller les notes avec un mot de passe (/!\ La note n'est pas chiffrée).
> Disponible sur F-Droid et sur Google Play


## Web
