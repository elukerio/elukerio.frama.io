# Proxy YouTube Invidious
[Invidious](https://github.com/omarroth/invidious) est une interface relais pour YouTube, ayant pour but de protéger la vie privée de ses utilisateurs. Il agit comme un intermédiaire ([proxy](https://fr.wikipedia.org/wiki/Proxy)) pour aller récupérer les vidéos YouTube tout en supprimant les traqueurs nuisibles.

## Notre instance
  * [yt.elukerio.org](https://yt.elukerio.org)
