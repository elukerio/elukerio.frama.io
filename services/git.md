# Gitea
Elukerio héberge sa propre forge basée sur [Gitea](https://gitea.io) à l'adresse [git.elukerio.org](https://git.elukerio.org). L'ensemble des projets d'Elukerio y est disponible (parfois sous forme de mirroir vers [framagit](https://framagit.org/elukerio)).

## Usages
Tous les adhérents d'Elukerio ont le droit d'avoir un compte sur [git.elukerio.org](https://git.elukerio.org).

Des dépots privés utiles à l'administration de l'association sont disponibles selon votre rôle.