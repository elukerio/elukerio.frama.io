# Métamoteur de recherche Searx
[Searx](https://github.com/asciimoo/searx) est un métamoteur de recherche libre, ayant pour but de protéger la vie privée de ses utilisateurs. Il agit comme un [proxy](https://fr.wikipedia.org/wiki/Proxy) pour aller récupérer les résultats sur de nombreux moteurs de recherche (liste disponible [ici](https://github.com/asciimoo/searx/wiki/possible-search-engines)).

## Nos instances
  * [searx.elukerio.org](https://searx.elukerio.org)
  * [searx.libmail.eu](https://searx.libmail.eu)

## Configuration du serveur web
Nous avons porté une attention particulière à la sécurité du site de nos instances searx. Les règles CSP et la configuration TLS ont été définies selon les préconisations de la fondation [Mozilla](https://www.mozilla.org/fr/). Il est possible que le service soit inaccessible sur les navigateurs web les plus anciens. Nos efforts sur la qualité de service et la sécurité permettent à nos instances [searx.elukerio.org](https://searx.elukerio.org), [searx.libmail.eu](https://searx.libmail.eu) de se hisser au sommet du classement mondial suivant : [stats.searx.xyz](https://stats.searx.xyz/).