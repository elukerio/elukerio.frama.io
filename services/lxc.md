# Serveurs personnels réservés aux adhérents

Les machines mises à disposition sont des conteneurs LXC hébergés sur les serveurs physiques d'Elukerio.

## Caractéristiques du service
### Adhésion "personne physique"
  * Domaine dédié : <user>.ext.elukerio.org (domaine personnel configurable sur demande à l'assistance)
  * CPU : 1 coeur
  * RAM : 512 MB
  * Connexion : jusqu'à 100 Mbit/s en symétrique
  * Proxy Web sur les domaines configurés (pour les modifications, conctater l'assistance)


### Adhésion "soutien" ou "personne morale"
Selon les besoins et le budget.

## Connexion aux machines
### Informations générales
Les conteneurs sont accessibles via SSH sur un port dédié qui est communiqué à chaque utilisateur.  
Voici un example pour l'utilisateur bob dont le port SSH est 9022 :  
```
bob@babar:~$ ssh -p9022 bob@bob.ext.elukerio.org
```

### Mises en garde
Nous recommandons fortement d'utiliser une paire de clés pour vous connecter. Vous pouvez suivre la procédure du wiki d'Archlinux [ici](https://wiki.archlinux.fr/Ssh#Cl.C3.A9_publique.2Fpriv.C3.A9e).

Ne changez pas le port SSH dans le fichier de configuration de votre machine, sinon vous perdrez l'accès à votre machine à cause de la configuartion du firewall en amont, vous ferez perdre du temps aux gentils adminsys ;-)
 
Vous êtes responsable de la sécurisation de votre machine. Il est indispensable pour vous d'utiliser un système de proctection tel que Fail2Ban pour éviter les attaques par force "brute". 

## Comment bien dialoguer avec l'assistance concernant ce service ?
Il est conseillé de donner un maximum d'informations dans ses messages "d'appel à l'aide":
* Nom d'utilisateur
* Domaine
* Port SSH
* Description détaillée du problème

En cherchant à décrire très précisément votre problème, parfois vous arriverez à le résoudre par vous-même, en tout cas vous aurez déjà beaucoup aidé les adminsys. Merci ! 

### Par Mail (adminsys (a) elukerio (point) org)
Vous pouvez envoyer un courriel aux administrateurs.

### Sur l'[agora](https://agora.elukerio.org) canal #Assistance
Ne contactez pas les adminsys en privé, cela empêche une bonne communication interne, rallonge les délais de prise en charge et surcharge certains bénévoles.

Un canal privé sur l'[agora](https://agora.elukerio.org) est prévu à cet effet : #Assistance.  

