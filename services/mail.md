# Service Mail
## Version Gratuite ([libmail.eu](https://www.libmail.eu))
Elukerio fournit gracieusement un serveur mail nommé [libmail.eu](https://www.libmail.eu) afin de faire découvrir les services de l'association. Étant donné que cette cette offre coûte de l'argent, le quota de stockage est très réduit **50 MB**, et le nombre mails envoyés est limité à 100/jour.   