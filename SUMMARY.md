# Summary

* [Accueil](README.md)

### Les guides
  * [Libérer votre smartphone (Android)](guides/liberer-votre-smartphone-android.md)
  * [Applications libres (Android)](guides/applications-libres.md)
  * [Auto-hébergement](guides/autohebergement.md)

### Les services
  * [Mail](services/mail.md)
  * [Cloud](services/nextcloud.md)
  * [Searx](services/searx.md)
  * [Invidious](services/invidious.md)
  * [Hébergement](services/lxc.md)
  * [Git](services/git.md)
  * [Jitsi](services/jitsi.md)
  * [Formulaires (alternative à GForms)](services/nextcloud_forms.md)
  * [Sondages (alternative à Doodle)](services/nextcloud_polls.md)
  * [En projet](services/en_projet.md)

### À propos
  * [Histoire de l'association](apropos/histoire.md)
  * [Notre philosophie](apropos/philo.md)
  * [Notre infrastructure](apropos/infrastructure.md)
