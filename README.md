# Documentation de l'association Elukerio (en Français)

## Présentation de l'association

Elukerio est une association initiée par trois jeunes lycéens en 2017, proposant aujourd’hui un large éventail de services basés sur des logiciels libres.

L'objectif est de reprendre le contrôle de sa vie numérique en misant sur le chiffrement, la transparence et la proximité tout en restant accessible au plus grand nombre du point de vue financier (réduction pour les étudiants et les personnes à faibles revenus).

## Objectif de ce wiki

Informer nos adhérents sur l'utilisation de nos services.
Vous aider à effectuer la transition vers du logiciel libre respectueux de votre vie privée.
Vous aider à configurer vos propres services et ainsi créer votre petit bout de l'Internet.

## Nous rejoindre

[Rejoindre Elukerio](https://www.elukerio.org)
## Contact

Par mail: [contact@elukerio.org](mailto:contact@elukerio.org)

## Contribuer

L'édition de cette documentation n'est pas ouverte à tous.
Cependant si vous souhaitez contribuer vous pouvez faire part de vos suggestions/articles en créant une nouvelle "Issue" sur le git de la documentation.
De même si vous décelez une faute d'orthographe.

[Créer une issue](https://framagit.org/elukerio/elukerio.frama.io/issues/new)

Merci à vous.
