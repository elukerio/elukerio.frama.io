# L'histoire d'Elukerio

## Notre origine

Association initiée par trois jeunes lycéens en 2017, proposant aujourd’hui un large éventail de services basés sur des logiciels libres.

L'objectif est de reprendre le contrôle de sa vie numérique en misant sur le chiffrement, la transparence et la proximité tout en restant accessible au plus grand nombre du point de vue financier (réduction pour les étudiants et les personnes à faibles revenus).

Nous étions il y a un an uniquement un service mail gratuit : [https://libmail.eu](https://libmail.eu). Ce service a été initié par Mablr le président de l'association ([voir l'article sur son blog](https://mablr.org/index.php?post/2018/06/29/Le-g%C3%A9n%C3%A8se-d-Elukerio...)).

## Remerciements

Un grand merci à [Alolise](https://alolise.org/) pour nous avoir donné ce superbe serveur et pour toute l'aide qu'ils nous ont apporté du point de vue technique (Merci @djayroma@framapiaf.org pour tous tes conseils et le temps que tu nous as consacré).

Un grand merci à [Rezopole](https://www.rezopole.net/fr/) pour l'hébergement qu'ils nous offrent car nous avons des valeurs communes.
Sans vous, notre décollage aurait été beaucoup plus long.