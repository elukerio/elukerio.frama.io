# La philosophie d'Elukerio

## Nos objectifs

### Sensibiliser et proposer des solutions simples

Sensibiliser à la protection de nos vies numériques et proposer des services libres et respectueux de la vie privée à nos utilisateurs.

### Éduquer

Nous souhaitons éduquer nos utilisateurs à la protection de leur vie privée.
Leur permettre une transition simplifiée vers des services libres et respectueux.
Et, pour les plus curieux, les former à la configuration de leurs propres services et éventuellement à la création de leur association. 
